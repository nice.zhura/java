## Solution packages

- [Linear programs](https://gitlab.com/nice.zhura/java/-/tree/master/src/modules/Basics_of_software_code_development/Linear_programs)
- [Branching](https://gitlab.com/nice.zhura/java/-/tree/master/src/modules/Basics_of_software_code_development/Branching)
- [Cycles](https://gitlab.com/nice.zhura/java/-/tree/master/src/modules/Basics_of_software_code_development/Cycles)


- [Return to the main directory](https://gitlab.com/nice.zhura/java)

## Tasks

- Practice (tasks).pdf - file contains a list of tasks for this module
