package modules.Basics_of_software_code_development.Linear_programs.Linear3;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Вычислить значение выражения по формуле (все переменные принимают действительные значения):
 * ( (sin(x) + cos(y)) / (cos(x) - sin(y)) ) * tan(x*y). */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Linear_programs/Linear3/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            double x = in.nextDouble();
            double y = in.nextDouble();

            // Вычисляем числитель выражения.
            double numerator = (Math.sin(x) + Math.cos(y)) * Math.tan(x * y);

            // Вычисляем знаменатель выражения.
            double denominator = Math.cos(x) - Math.sin(y);

            // Вычисляем значение выражения по формуле.
            double result = numerator / denominator;

            // Выводим начальные данные.
            System.out.println("Initial variables:");
            System.out.println("x: " + x);
            System.out.println("y: " + y);
            System.out.println();

            // Выводим вычисленное значение.
            System.out.println("Result:");
            System.out.println("var: " + result);
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
