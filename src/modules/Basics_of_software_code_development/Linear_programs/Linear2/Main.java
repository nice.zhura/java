package modules.Basics_of_software_code_development.Linear_programs.Linear2;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Вычислить значение выражения по формуле (все переменные принимают действительные значения):
 * (b + sqrt(b^{2} + 4*a*c)) / (2*a) - a^{3}*c + b^{-2}. */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Linear_programs/Linear2/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            double a = in.nextDouble();
            double b = in.nextDouble();
            double c = in.nextDouble();

            // Вычисляем выражение под конем из формулы.
            double discriminant = b * b + 4 * a * c;

            // Проверка на отрицательность дискриминанта.
            if (discriminant < 0.0) {
                throw new IllegalArgumentException("Discriminant less than zero!");
            } else {
                // Проверка на деление на 0.
                if (a == 0.0 || b == 0.0) {
                    throw new IllegalArgumentException("Division by zero!");
                } else {
                    // Вычисляем значение выражения по формуле.
                    double result = ((b + discriminant) / (2.0 * a)) - Math.pow(a, 3.0) * c + Math.pow(b, -2.0);

                    // Выводим начальные данные.
                    System.out.println("Initial variables:");
                    System.out.println("a: " + a);
                    System.out.println("b: " + b);
                    System.out.println("c: " + c);
                    System.out.println();

                    // Выводим вычисленное значение.
                    System.out.println("Result:");
                    System.out.println("var: " + result);
                }
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            // Обрабатываем ошибки при вычислении выражения.
            System.err.println("IllegalArgumentException:");
            System.err.println(ex.getMessage());
        }
    }
}
