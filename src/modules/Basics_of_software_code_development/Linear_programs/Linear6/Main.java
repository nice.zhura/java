package modules.Basics_of_software_code_development.Linear_programs.Linear6;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Для данной области составить линейную программу, которая печатает true,
 * если точка с координатами (х, у) принадлежит закрашенной области,
 * и false — в противном случае. */

// Предполагаю, что одна клетка - 1 ед. деления,
// а также, что граница фигур есть закрашенная область.

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Linear_programs/Linear6/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            double x = in.nextDouble();
            double y = in.nextDouble();

            // Проверка попадания точки в эллипс.
            boolean inEllipse = (Math.pow((x / 0.5), 2.0) + Math.pow(((y + 1) / 0.75), 2.0)) < 1.0;

            // Проверка попадания точки в квадрат.
            boolean inSquare = (Math.abs(x) <= 2.0 && (y >= 0.0 && y <= 4.0));

            // Проверка попадания точки в прямоугольник.
            boolean inRectangle = (Math.abs(x) <= 4.0 && (y >= -3.0 && y <= 0.0));

            // Проверка попадания точки в закрашенную область.
            boolean inFigure = (inRectangle && !inEllipse) || inSquare;

            // Выводим начальные данные.
            System.out.printf("(x, y): (%.2f, %.2f) \n", x, y);

            // Выводим полученный результат.
            System.out.println("(x, y) in figure? " + inFigure);
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
