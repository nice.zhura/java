package modules.Basics_of_software_code_development.Linear_programs.Linear5;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Дано натуральное число Т, которое представляет длительность прошедшего времени в секундах.
 * Вывести данное значение длительности в часах, минутах и секундах в следующей форме:
 * ННч ММмин SSc. */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Linear_programs/Linear5/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            int T = in.nextInt();

            // Вычисляем число часов, минут, секунд.
            int hours = T / 3600;
            int minutes = (T - hours * 3600) / 60;
            int seconds = T - minutes * 60 - hours * 3600;

            // Выводим начальные данные.
            System.out.println("Initial variable:");
            System.out.printf("T: %dc \n\n", T);

            // Выводим полученный результат в требуемом виде.
            System.out.println("Result:");
            System.out.printf("%02dч %02dмин %02dc", hours, minutes, seconds);
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
