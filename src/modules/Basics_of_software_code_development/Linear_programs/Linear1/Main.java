package modules.Basics_of_software_code_development.Linear_programs.Linear1;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

// Найдите значение функции z = ((a - 3) * b / 2) + c.

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Linear_programs/Linear1/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            double a = in.nextDouble();
            double b = in.nextDouble();
            double c = in.nextDouble();

            // Вычисляем значение z по формуле.
            double z =  ((a - 3.0) * b / 2.0) + c;

            // Выводим начальные данные.
            System.out.println("Initial variables:");
            System.out.println("a: " + a);
            System.out.println("b: " + b);
            System.out.println("c: " + c);
            System.out.println();

            // Выводим вычисленное значение.
            System.out.println("Result:");
            System.out.println("z: " + z);
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем оставшиеся ошибки.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
