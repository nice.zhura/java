package modules.Basics_of_software_code_development.Cycles.Cycles8;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Даны два числа. Определить цифры, входящие в запись
 * как первого так и второго числа. */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Cycles/Cycles8/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            int firstNumber = in.nextInt();
            int lastNumber = in.nextInt();

            // Выводим начальные данные.
            System.out.println("Initial variables:");
            System.out.println("first number: " + firstNumber);
            System.out.println("last number: " + lastNumber);
            System.out.println();

            int[] sameDigits = new int[10];
            boolean haveSameDigits = false;

            // Вычисляем число общих цифр двух чисел.
            while (firstNumber > 0) {
                int rem = firstNumber % 10;
                firstNumber /= 10;

                int temp = lastNumber;
                while (temp > 0) {
                    if (temp % 10 == rem) {
                        sameDigits[rem]++;
                        haveSameDigits = true;
                        break;
                    }
                    temp /= 10;
                }
            }

            // Выводим результат.
            System.out.println("Result: ");
            if (haveSameDigits) {
                System.out.println("Same digits: ");
                for (int i = 0; i < 10; ++i) {
                    if (sameDigits[i] > 0) {
                        System.out.print(i + " ");
                    }
                }
            } else {
                System.out.println("Numbers don't have same digits");
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
