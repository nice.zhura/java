package modules.Basics_of_software_code_development.Cycles.Cycles4;

import java.math.BigInteger;

// Составить программу нахождения произведения
// квадратов первых двухсот чисел.

public class Main {
    public static void main(String[] args) {
        // Вычисляем произведение квадратов первых 200 чисел:
        BigInteger product = new BigInteger("1");
        for (int i = 2; i <= 200; ++i) {
            product = product.multiply(BigInteger.valueOf(i * i));
        }

        // Выводим результат.
        System.out.println("The product of the squares of the first two hundred numbers: ");
        System.out.println(product);
    }
}
