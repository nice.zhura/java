package modules.Basics_of_software_code_development.Cycles.Cycles2;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Вычислить значения функции на отрезке [a, b] с шагом h:
 * f(x) = x, x > 2
 * f(x) = -x, x <= 2
 */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Cycles/Cycles2/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем границы отрезка.
            int a = in.nextInt();
            int b = in.nextInt();

            if (a > b) {
                throw new IllegalArgumentException("Invalid variables a, b: a > b ?!");
            }

            // Получаем шаг функции.
            int h = in.nextInt();

            if (h < 0) {
                throw new IllegalArgumentException("Invalid variable h: h < 0 ?!");
            }

            // Выводим начальные данные.
            System.out.printf("[a, b]: [%d, %d] \n", a, b);
            System.out.printf("increment h: %d \n", h);
            System.out.println();

            // Выводим результат.
            System.out.println("Result:");
            for (int x = a; x <= b; x += h) {
                if (x > 2) {
                    System.out.printf("x: %2d, f(x): %2d", x, x);
                } else {
                    System.out.printf("x: %2d, f(x): %2d", x, -x);
                }
                System.out.println();
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            // Обрабатываем ошибки неверных начальных данных.
            System.err.println("IllegalArgumentException:");
            System.err.println(ex.getMessage());
        }
    }
}
