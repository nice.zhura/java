package modules.Basics_of_software_code_development.Cycles.Cycles6;

/* Вывести на экран соответствий между символами и их
 * численными обозначениями в памяти компьютера. */

// Полагаю что просят вывести таблицу ASCII.
// Вывожу с 33 номера, так как до него, начиная с 0
// символы не отображаются корректно.

public class Main {
    public static void main(String[] args) {
        // Выводим соответствия между символами и их численными обозначениями.
        System.out.printf("%14s: %10s: \n", "Decimal number", "Symbol");
        for (int i = 33; i <= 255; ++i) {
            System.out.printf("%-3d %17s", i, (char)i);
            System.out.println();
        }
    }
}
