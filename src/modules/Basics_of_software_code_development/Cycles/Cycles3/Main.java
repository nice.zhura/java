package modules.Basics_of_software_code_development.Cycles.Cycles3;

// Найти сумму квадратов первых ста чисел.

public class Main {
    public static void main(String[] args) {
        // Длина ряда квадратов натуральных чисел.
        int n = 100;

        // Вычисляем сумму квадратов первых ста чисел:
        int sum = n * (n + 1) * (2 * n + 1) / 6;

        // Выводим результат.
        System.out.println("The sum of the squares of the first hundred numbers: ");
        System.out.println("Sum: " + sum);
    }
}
