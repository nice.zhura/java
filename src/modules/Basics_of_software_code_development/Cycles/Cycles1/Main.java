package modules.Basics_of_software_code_development.Cycles.Cycles1;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Напишите программу, где пользователь вводит любое целое положительное число.
 * А программа суммирует все числа от 1 до введенного пользователем числа */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Cycles/Cycles1/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            int n = in.nextInt();

            // Вычисляем сумму первых n чисел.
            int sum = (n + 1) * n / 2;

            // Выводим начальные данные.
            System.out.println("Initial variable:");
            System.out.println("n: " + n);
            System.out.println();

            // Выводим результат.
            System.out.println("Result:");
            System.out.println("sum: " + sum);
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
