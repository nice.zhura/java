package modules.Basics_of_software_code_development.Cycles.Cycles7;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Для каждого натурального числа в промежутке от m до n вывести все делители,
 * кроме единицы и самого числа. m и n вводятся с клавиатуры. */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Cycles/Cycles7/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            int m = in.nextInt();
            int n = in.nextInt();

            // Обрабатываем неверные границы числового промежутка.
            if (m > n) {
                throw new IllegalArgumentException("Invalid variables a, b: a > b ?!");
            }

            // Выводим начальные данные.
            System.out.println("Initial variables:");
            System.out.printf("[m, n]: [%d, %d] \n", m, n);
            System.out.println();

            // Выводим результат.
            System.out.println("Result: ");
            for (int var = m; var <= n; ++var) {
                System.out.println("var: " + var);

                boolean hasDivider = false;

                System.out.print("div: ");
                for (int div = 2; div < var; ++div) {
                    if (var % div == 0) {
                        System.out.print(div + " ");
                        hasDivider = true;
                    }
                }

                // Выводим сообщение об остутствии нужных делителей.
                if (!hasDivider) {
                    System.out.println("there are no divisors other than 1 and " + var + "\n");
                } else {
                    System.out.print("\n\n");
                }
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            // Обрабатываем ошибки неверных начальных данных.
            System.err.println("IllegalArgumentException:");
            System.err.println(ex.getMessage());
        }
    }
}
