package modules.Basics_of_software_code_development.Cycles.Cycles5;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Даны числовой ряд и некоторое число e. Найти сумму тех членов ряда,
 * модуль которых больше или равен заданному e. Общий член ряда имеет вид:
 * a_{n} = \frac{1}{2^{n}} + \frac{1}{3^{n}} */

// Полагаю что это числовой ряд и сумма начинается от n = 1
// и соотвественно сумма ряда при n -> infinity равна 1.5
// как предел суммы бесконечно убывающих последовательностей.

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Cycles/Cycles5/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            double eps = in.nextDouble();

            // Выводим результат
            System.out.println("Result:");
            if (eps > 1.5) {
                System.out.println("eps is greater than the infinite sum of the series");
            } else if (eps == 1.5) {
                System.out.println("sum is equal to eps := 1.5");
            } else {
                double firstSum = 1.0 / 2.0;
                double secondSum = 1.0 / 3.0;

                double sum = firstSum + secondSum;
                while (sum < eps) {
                    firstSum /= 2.0;
                    secondSum /= 3.0;
                    sum += (firstSum + secondSum);
                }
                System.out.println("eps: " + eps);
                System.out.println("sum: " + sum);
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            // Обрабатываем ошибки неверных начальных данных.
            System.err.println("IllegalArgumentException:");
            System.err.println(ex.getMessage());
        }
    }
}
