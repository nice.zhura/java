package modules.Basics_of_software_code_development.Branching.Branching1;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Даны два угла треугольника (в градусах).
 * Определить, существует ли такой треугольник, и если да,
 * то будет ли он прямоугольным. */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Branching/Branching1/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            double firstAngle = in.nextDouble();
            double secondAngle = in.nextDouble();

            // Проверяем существование треугольника.
            boolean isExists = (firstAngle > 0.0 && secondAngle > 0.0) && ((firstAngle + secondAngle) < 180.0);

            // Проверяем является ли треугольник прямоугольным.
            boolean isRight = (firstAngle == 90.0 || secondAngle == 90.0 || (firstAngle + secondAngle) == 90.0);

            // Выводим начальные данные.
            System.out.println("Initial variables:");
            System.out.println("first angle: " + firstAngle);
            System.out.println("second angle: " + secondAngle);
            System.out.println();

            // Выводим результат.
            System.out.println("Result:");
            if (isExists) {
                System.out.println("A triangle with given angles exists.");
                if (isRight) {
                    System.out.println("A triangle is right-angled.");
                }
            } else {
                System.out.println("A triangle with given angles doesn't exist.");
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
