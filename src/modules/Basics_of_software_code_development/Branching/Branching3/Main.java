package modules.Basics_of_software_code_development.Branching.Branching3;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/*  Даны три точки А(х1,у1), В(х2,у2) и С(х3,у3).
 *  Определить, будут ли они расположены на одной прямой. */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Branching/Branching3/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем координаты точки А(х1,у1).
            double x1 = in.nextDouble();
            double y1 = in.nextDouble();

            // Получаем координаты точки В(х2,у2).
            double x2 = in.nextDouble();
            double y2 = in.nextDouble();

            // Получаем координаты точки С(х3,у3).
            double x3 = in.nextDouble();
            double y3 = in.nextDouble();

            /* Проверяем расположение точек на одной прямой,
             * построив уравнение прямой в каноническом виде по точкам А(х1,у1), В(х2,у2)
             * и подставив в него точку С(х3,у3). */
            boolean onOneLine = ((x3 - x1) * (y2 - y1) == (y3 - y1) * (x2 - x1));

            // Выводим начальные данные.
            System.out.println("Initial variables:");
            System.out.printf("A(x1, y1): (%.2f, %.2f) \n", x1, y1);
            System.out.printf("A(x2, y2): (%.2f, %.2f) \n", x2, y2);
            System.out.printf("A(x3, y3): (%.2f, %.2f) \n", x3, y3);
            System.out.println();

            // Выводим итоговый результат.
            if (onOneLine) {
                System.out.println("The points lie on one straight line.");
            } else {
                System.out.println("The points don't lie on one straight line.");
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
