package modules.Basics_of_software_code_development.Branching.Branching5;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/* Вычислить значение функции:
 * f(x) = x^{2} - 3x + 9, x <= 3
 * f(x) = \frac{1}{(x^{3} + 6)}, x > 3
 */

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Branching/Branching5/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем начальные данные.
            double x = in.nextDouble();

            // Вычисляем значение функции в точке.
            double result;

            if (x <= 3.0) {
                result = x * x - 3.0 * x + 9.0;
            } else {
                result = 1.0 / (Math.pow(x, 3.0) + 6.0);
            }

            // Выводим начальные данные.
            System.out.println("Initial variable:");
            System.out.println("x: " + x);
            System.out.println();

            // Выводим результат.
            System.out.println("Result:");
            System.out.println("f(x): " + result);
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        }
    }
}
