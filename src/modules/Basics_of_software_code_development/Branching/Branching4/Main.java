package modules.Basics_of_software_code_development.Branching.Branching4;

import java.io.FileNotFoundException;
import java.io.FileInputStream;

import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/*  Заданы размеры А, В прямоугольного отверстия и размеры х, у, z кирпича.
 *  Определить, пройдет ли кирпич через отверстие. */

// Полагаю, что кирпич можно поместить в отверстие
// либо вертикально, либо горизонтально.

public class Main {
    public static void main(String[] args) {
        try {
            // Перенаправляем поток ввода на файл input.txt.
            String path = "src/modules/Basics_of_software_code_development/Branching/Branching4/input.txt";
            System.setIn(new FileInputStream(path));

            // Создаем переменную Scanner для
            // считывания переменных из файла.
            Scanner in = new Scanner(System.in);

            // Получаем размеры А, В прямоугольного отверстия.
            double A = in.nextDouble();
            double B = in.nextDouble();

            // Получаем размеры х, у, z кирпича.
            double x = in.nextDouble();
            double y = in.nextDouble();
            double z = in.nextDouble();

            // Проверяем длины фигур на положительность.
            boolean isLegal = (A > 0.0 && B > 0.0 && x > 0.0 && y > 0.0 && z > 0.0);

            // Проверяем помещается ли плоскость xy кирпича.
            boolean xyPlaneFits = (x <= A && y <= B) || (x <= B && y <= A);

            // Проверяем помещается ли плоскость xz кирпича.
            boolean xzPlaneFits = (x <= A && z <= B) || (x <= B && z <= A);

            // Проверяем помещается ли плоскость yz кирпича.
            boolean yzPlaneFits = (y <= A && z <= B) || (z <= B && y <= A);

            // Выводим начальные данные.
            System.out.println("Initial variables:");
            System.out.printf("A: %.2f \n", A);
            System.out.printf("B: %.2f \n", B);
            System.out.printf("x: %.2f \n", x);
            System.out.printf("y: %.2f \n", y);
            System.out.printf("z: %.2f \n", z);
            System.out.println();

            // Выводим итоговый результат.
            if (isLegal) {
                System.out.println("Result:");
                if (xyPlaneFits || xzPlaneFits || yzPlaneFits) {
                    System.out.println("The brick will go through the hole.");
                } else {
                    System.out.println("The brick won't go through the hole.");
                }
            } else {
                throw new IllegalArgumentException("The lengths of the shapes have negative values.");
            }
        } catch(FileNotFoundException ex) {
            // Обрабатываем ошибку отсутствия файла.
            System.err.println("FileNotFoundException:");
            System.err.println(ex.getMessage());
        } catch(InputMismatchException ex) {
            // Обрабатываем ошибку неправильного ввода переменных.
            System.err.println("InputMismatchException:");
            System.err.println(ex.getMessage());
        } catch (NoSuchElementException ex) {
            // Обрабатываем ошибку отсутствия ввода переменных.
            System.err.println("NoSuchElementException:");
            System.err.println(ex.getMessage());
        } catch (IllegalStateException ex) {
            // Обрабатываем ошибку неправильного состояния scanner.
            System.err.println("IllegalStateException:");
            System.err.println(ex.getMessage());
        } catch (IllegalArgumentException ex) {
            // Обрабатываем ошибку неверных начальных данных.
            System.err.println("IllegalArgumentException:");
            System.err.println(ex.getMessage());
        }
    }
}
